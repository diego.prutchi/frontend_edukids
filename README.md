# Projeto 03 - Frontend - EDUKIDS

Sistema feito como terceiro desafio técnico do Bootcamp de Full Stack do Instituto Infnet.
É um site de vídeos direcionados ao público infantil, com acesso supervisionado por adultos, que definem quais vídeos cada perfil vinculado pode ter acesso. O usuário realiza o próprio cadastro e o de cada um de seus filhos. Existem ambientes separados para cada perfil com permissões correspondentes. Há autenticação por login e senha e geração de Token para controle de navegação.

## APIs e BDs

Uma API foi criada e é consumida por este Frontend. Ela está disponível em: <br><br>
https://api-edukids.herokuapp.com/ <br>
https://gitlab.com/luiz.marques/backend-edukids <br>

O BD escolhido foi o Postgres e a hospedagem do mesmo encontra-se no ElephantSQL.

## Autores

- [Diego Prutchi](https://linkedin.com/in/diegoprutchi)
- [Karine Paiva](https://www.linkedin.com/in/karinepaiva)
- [Luiz Marques](https://www.linkedin.com/in/luiz-marques-a4307648/)

  
## Live Demo
https://edukids.vercel.app/


## Tecnologias

Frontend feito em NodeJS + ReactJS com o uso das seguintes "libs":
- Reach Router
- Ant Design
- Axios
- Dot Env
- JSON Web Token
- Styled Components
- Redux
  
## Feedback

Será muito apreciado. Por favor envie para:

diego.prutchi@al.infnet.edu.br <br>
karine.paiva@al.infnet.edu.br <br>
luiz.marques@al.infnet.edu.br

  
## Execute o projeto localmente

Clone o projeto

```bash
  git clone https://gitlab.com/diego.prutchi/frontend_edukids.git
```

Vá para a pasta do projeto

```bash
  cd <nome_pasta>
```

Instale as dependências

```bash
  yarn install
```

Inicie o Servidor

```bash
  yarn start
```

  
