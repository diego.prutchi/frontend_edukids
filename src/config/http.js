import axios from "axios";
import { getToken } from "./storage";
import store from "../store/index";
import { logoutAuth } from "../store/login/action";

//process.env.REACT_APP_API 

const urlDefault = process.env.REACT_APP_API || "http://localhost:3333";

const http = axios.create({
  baseURL: urlDefault,
});

// re hydrate token 
if (getToken()) {
  http.defaults.headers["Authorization"] = `bearer ${getToken()}`;
}

http.defaults.headers["content-type"] = "application/json";


http.interceptors.response.use(
  (response) => response,
  (error) => {
    switch (error.response.status) {
      case 401:
        store.dispatch(logoutAuth());
        return Promise.reject(error);
      default:
        return Promise.reject(error);
    }
  }
);



export default http;
