import { navigate } from "@reach/router";
// import { toast } from "react-toastify";
import http from "../../config/http";
import { removeToken, saveToken } from "../../config/storage";
import { authServiceLogin } from "../../services/service.login";
import { LOGIN } from "../types";
// import _ from "lodash";

export const authLogin = (data) => {
  return async (dispatch, getState) => {
    // dispatch({ type: AUTH.loading, data: true });
    try {
      // Obtem os dados de dentro da api do backend através do servico authServiceLogin
      const response = await authServiceLogin(data);

      // salva os dados do response no localstorage atraves da funcã́o saveToken
      saveToken(response.data.token);

      // defino como padrão nas proximas requests que o header vai ter o Authorization com token
      http.defaults.headers["Authorization"] = `bearer ${response.data.token}`;

      // mando para o Store as informaçoes de token
      dispatch({ type: LOGIN.LOGIN, data: response.data });

      // obtem as informaçoes guardadas dentro da store, e faz a tomada de decisão da saida de rotas
      // const routerOutput = getState().auth.type.route;

      // funçãoo de redirect para rota especificada
      navigate("/videos");

      // Enviar um dispatch para a store e desabilitar o loading
      // dispatch({ type: AUTH.loading, data: false });
    } catch (error) {

      alert('Usuário ou senha incorretos!')
      // dispatch({ type: AUTH.loading, data: false });

      // if (_.isArray(error.response)) {
      //   error.response?.data?.errors.map((erro) => toast.error(erro.msg));
      // } else {
      //   toast.error(error.response.data.error);
      // }
    }
  };
};

export const logoutAuth = (data) => {
  return async (dispatch) => {
    try {
      removeToken();
      dispatch({ type: LOGIN.LOGOUT });
      navigate("/");
    } catch (error) {
      dispatch({ type: LOGIN.LOADING, data: false });
    }
  };
};
