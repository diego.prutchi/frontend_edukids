import { applyMiddleware, combineReducers, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"
import videoReducer from "./videos/reducer" 
import loginReducer from "./login/reducer" 
import modalReducer from "./modal/reducer" 
import kidsReducer from "./kids/reducer" 

const reducers = combineReducers ({ video: videoReducer, login: loginReducer, modal: modalReducer, kids: kidsReducer })

const middlewares = [thunk]

const compose = composeWithDevTools (applyMiddleware(...middlewares))

const store = createStore(reducers, compose)

export default store