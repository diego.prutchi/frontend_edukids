import { VIDEOS } from "../types"
import { videosGetAll, getAllVideos } from "../../services/service.videos"

export const actionVideosGetAll = () => {
  
  return async(dispatch) => {
    const result = await videosGetAll ()
  
    //ordenando pelo parental guide
    const sortParentalGuide = ((a, b) => {
   
      if (a.videos.pg > b.videos.pg) {
        return 1;
      }
      if (a.videos.pg < b.videos.pg) {
        return -1;
      }
      return 0;
    })
    //montando o objeto separado por parental guide
    const videosByParentalGuide = result.data.sort(sortParentalGuide).reduce((acc, data) => {
      return {
        ...acc,
        [data.parentalGuide] : [
          ...(acc[data.parentalGuide] || []),
          data.videos
        ],
      }
    },{})

    dispatch ({
      type: VIDEOS.VIDEOS_GET_ALL,
      data: videosByParentalGuide
    })
  }
}
export const getVideos = () => {
  
  return async(dispatch) => {
    const result = await getAllVideos()
    
    //ordenando pelo parental guide
    const sortParentalGuide = ((a, b) => {
   
      if (a.pg > b.pg) {
        return 1;
      }
      if (a.pg < b.pg) {
        return -1;
      }
      return 0;
    })
    //montando o objeto separado por parental guide
    const videosByParentalGuide = result.data.sort(sortParentalGuide).reduce((acc, data) => {
      return {
        ...acc,
        [data.parentalGuide] : [
          ...(acc[data.parentalGuide] || []),
          data
        ],
      }
    },{})

    dispatch ({
      type: VIDEOS.GET_VIDEOS,
      data: videosByParentalGuide
    })
  }
}