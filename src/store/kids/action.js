import { KIDS } from "../types"
import { getAllKids } from "../../services/service.kids"


export const getKids = () => {
  return async (dispatch) => {
    const kids = await getAllKids();
    dispatch({ type: KIDS.KIDS_GET_ALL, data: kids.data });
  };
};

export const setKid = (kid) => {

  return (dispatch) => {
    dispatch({type: KIDS.SET_KID, data: kid })
  }

}