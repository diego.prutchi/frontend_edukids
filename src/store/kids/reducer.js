import { KIDS } from "../types";

const INITIAL_STATE = {
  kids: [],
  profile: {}
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case KIDS.KIDS_GET_ALL:
      return { ...state, kids: action.data };
    case KIDS.SET_KID:
      return { ...state, profile: action.data };
    default:
      return state;
  }
};

export default reducer;
