import Home from './views/Home';
import MyVideos from './views/MyVideos';
import Users from './views/Users';
import Login from './views/Login';
import Plans from './views/Plans';
import { Videos } from './views/Videos';
import CreateUsers from './views/CreateUsers';
import AccountUser from './views/AccountUser';
import CreateKid from './views/CreateKids';
import AccountProfiles from './views/AccountKid';
import { KidsVideos } from './views/KidsVideos';
import Layout from './components/portal/layout/index';
import { Router, Redirect } from '@reach/router';
import { isAuthenticated } from "./config/storage"
import AddVideos from './views/AddVideos';

const Routers = () => {
  const PrivateRouter = ({ component: Component, ...rest }) => {
    const isLogin = rest.path === "/login";
    // const userRoleRoute = useSelector((state) => state.auth?.type.route);

    if (!isAuthenticated() && !isLogin) {
      return <Redirect to="/login" noThrow />;
    }
    if (isAuthenticated() && isLogin) {
      return <Redirect to={"/videos"} noThrow />;
    }
    return <Component {...rest} />;
  };

  return (

    <Layout>

      <Router>

        <Home path="/" />
        <Login path="/login" />
        <CreateUsers path="/create/users" />
        <PrivateRouter path="/myVideos" component={MyVideos} />
        <PrivateRouter path="/users" component={Users} />
        <PrivateRouter path="/videos" component={Videos} />
        <PrivateRouter path="/user/new" component={CreateKid} />
        <PrivateRouter path="/user/profile/edit" component={AccountProfiles} />
        <PrivateRouter path="/plans" component={Plans} />
        <PrivateRouter path="/user/profile" component={KidsVideos} />
        <PrivateRouter path="/myAccount" component={AccountUser} />
        <PrivateRouter path="/id" component={AccountProfiles} />
        <PrivateRouter path="/adm/videos" component={AddVideos} />

      </Router>

    </Layout>
  );
};

export default Routers;