import React from 'react'
import { useState } from "react";
import { Form, Input, Button, Col } from 'antd';
import { useDispatch } from "react-redux";
import { authLogin } from "../store/login/action"



function Login() {
  const [form, setForm] = useState({})
  const dispatch = useDispatch()
  const handleChange = (event) => {
    const { name, value } = event.target;
    console.log(event)
    setForm({
      ...form,
      [name]: value,
    });
  };

  const submitLogin = () => dispatch(authLogin(form));

  return (
    <div className="formLogin">
      <Col sm={14} lg={8} md={3}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        autoComplete="off"
      >
        <Form.Item
          label="Email"
          rules={[{ required: true, message: 'Digite seu email' }]}
        >
          <Input
            onChange={handleChange}
            value={form.email || ""}
            name="email"
          />
        </Form.Item>

        <Form.Item
          label="Password"
          rules={[{ required: true, message: 'Digite sua senha' }]}
        >
          <Input.Password
            onChange={handleChange}
            value={form.password || ""}
            name="password"
          />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            onClick={submitLogin}
            type="primary" htmlType="submit">
            Enviar
          </Button>

        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }} nameClass="linkCreate">
          <a href="/create/users" nameClass="buttonLoginCreate">
            Não tem conta? Clique aqui para criar!
          </a>
        </Form.Item>
      </Form>
      </Col>
    </div>
  )
}

export default Login
