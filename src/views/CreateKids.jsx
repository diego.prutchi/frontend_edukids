import { Input, Form, Button } from 'antd';
import { createKid } from '../services/service.kids';
import { useState } from 'react';
import { navigate } from "@reach/router";
import '../components/portal/layout/style.css'



const CreateKid = () => {

  const [formKid, setFormKid] = useState({})

  const handleChange = (event) => {

    const { name, value } = event.target

    setFormKid({
      ...formKid,
      [name]: value

    })

  }

  const handleSubmit = async () => {

    await createKid(formKid)
    navigate('/videos', alert ('Perfil criado com sucesso!'))

  }

  return (
    <div className="form">
      
        <Form.Item name="nickname" label="Nickname" rules={[
          {
            required: true,
            message: 'Por favor, digite o apelido.',
            whitespace: true,
          },
        ]}>
          <Input name="nickname" name="nickname" value={formKid.nickname || ""} onChange={handleChange} />
        </Form.Item>

        <Form.Item name="age" label="Age" rules={[
          {
            type: 'number',
            required: true,
            message: 'Por favor, digite a idade.',
          },
        ]}>
          <Input name="age" value={formKid.age || ""} onChange={handleChange}/>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" onClick={handleSubmit}>
            Criar
          </Button>
        </Form.Item>
        
    </div >
  )
}

export default CreateKid;