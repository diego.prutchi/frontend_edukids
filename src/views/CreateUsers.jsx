import { Form, Input, Button, DatePicker } from 'antd';
import { createUser } from '../services/services.createuser';
import { useState } from 'react';
import { navigate } from "@reach/router";
import '../components/portal/layout/style.css'


const CreateUsers = () => {

  const [formUser, setFormUser] = useState({})
  const [date, setDate] = useState('')


  const handleChange = (event) => {

    const { name, value } = event.target

    setFormUser({
      ...formUser,
      [name]: value,
      birthday: date
    })

  }

  function onChange(date, dateString) {
    setDate(dateString)
  }

  const handleSubmit = async () => {

    await createUser(formUser)
    navigate('/videos', alert ('Bem vindo(a)!'))
  }

  return (
    <div className="form">

      

      <Form.Item name="name" label="Nome completo" rules={[
        {
          required: true,
          message: 'Por favor, digite seu nome completo',
          whitespace: true,
        },
      ]}>
        <Input name="name" value={formUser.name || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="email" label="E-mail" rules={[
        {
          type: 'email',
          message: 'Email não validado!',
        },
        {
          required: true,
          message: 'Por favor, digite seu email!',
        },
      ]}>
        <Input name="email" value={formUser.email || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="birthday" label="Data de Nascimento">
        <DatePicker name="birthday" value={formUser.birthday || ""} onChange={onChange}/>
      </Form.Item>

      <Form.Item name="cpf" label="CPF" rules={[
        {
          type: 'number',
          required: true,
          message: 'Por favor, digite seu CPF!',
        },
      ]}>
        <Input name="cpf" value={formUser.cpf || ""} onChange={handleChange} />
      </Form.Item>

      <Form.Item name="phone" label="Celular" rules={[
          {
            required: true,
            message: 'Por favor, digite seu número do celular!',
          },
        ]}>
        <Input name="phone" value={formUser.phone || ""} onChange={handleChange}
          style={{
            width: '50%',

          }}
        /></Form.Item>

      <Form.Item
        name="address"
        label="Endereço"
        rules={[
          {
            required: true,
            message: 'Por favor, digite seu endereço!',
          },
        ]}><Input name="address" value={formUser.address || ""} onChange={handleChange} /></Form.Item>

      <Form.Item
        name="password"
        label="Senha"
        rules={[
          {
            required: true,
            message: 'Por favor, digite sua senha.',
          },
        ]}
        hasFeedback
      ><Input.Password /></Form.Item>

      <Form.Item
        name="confirm"
        label="Confirme sua senha."
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Por favor, confirme sua senha.',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject(new Error('As senhas não estão iguais!'));
            },
          }),
        ]}><Input.Password name="password" value={formUser.password || ""} onChange={handleChange} /></Form.Item>


      <Form.Item>
        <Button type="primary" htmlType="submit" onClick={handleSubmit}>
          Cadastrar
        </Button>
      </Form.Item>

      <Form.Item>

        <a href="/login">
          Já tem conta? Clique aqui para entrar!
        </a>

      </Form.Item>
    </div>
  )
}

export default CreateUsers;