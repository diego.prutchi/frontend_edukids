import { Link } from '@reach/router'
import '../components/portal/layout/style.css'
import { Layout, Menu, Breadcrumb, Button } from 'antd';
import {navigate} from "@reach/router"

import Bg1 from "../assets/images/bg1.jpeg"
import MockHome from "../assets/images/mockHome.jpeg"

const Home = () => {

  const { Header, Content, Footer } = Layout;

  return (

    <Content className="site-layout-background" style={{ padding: 24, minHeight: 720 }}>
      <div className="containerHome">
        <div className="title">
          <h2>EDUKIDS. Videos seguros para seus filhos</h2>
        </div>
      </div>
        <Button type="primary" onClick={()=>{navigate("/create/users")}} block>
          Cadastre-se!
        </Button>
        <div className="mock">
          <img src={MockHome} />
        </div>
    </Content>

  )
}

export default Home;