import { Form, Input, Button } from 'antd';
import { dadosKid, updateKid, deleteKid } from '../services/service.kids';
import { useDispatch, useSelector } from 'react-redux'
import { useState, useEffect } from 'react';
import { navigate } from '@reach/router';
import Modal from "../components/modal"
import { logoutAuth } from "../store/login/action"



const AccountKid = () => {

  const dispatch = useDispatch()
  const [formUpdateKid, setUpdateFormKid] = useState({})
  const stateKid = useSelector(state => state.login.user)
  const [open, setOpen] = useState(false)

  const handleChange = (event) => {

    const { name, value } = event.target

    setUpdateFormKid({
      ...formUpdateKid,
      [name]: value,
    })

  }

  useEffect(() => {
    dadosKid(stateKid.id).then((result) => {
      setUpdateFormKid(result.data)
    }).catch((error) => {
      alert(error.message)
    })
  }, [])

  const logout = () => {
    dispatch(logoutAuth())
    setOpen(false)
  }

  const handleSubmit = async () => {

    try {

      await updateKid(stateKid.id, formUpdateKid)
      alert('Usuário atualizado com sucesso!')

    } catch (error) {
      alert('Usuário não atualizado, tente novamente.')
    }

  }

  const handleDelete = async () => {

    try {
      await deleteKid(stateKid.id, deleteKid)
      navigate('/', alert('Perfil excluído com sucesso.'))
    } catch (error) {
      alert('Não foi possível excluir sua conta, tente novamente.')
    }

  }

  const deletedKid = () => {
    logout();
    handleDelete();
  }

  return (

    <div className="form">

      <Input name="nickname" value={formUpdateKid.nickname || ""} onChange={handleChange} />

      <Input name="age" value={formUpdateKid.age || ""} onChange={handleChange} />

      <Form.Item>
        <Button type="primary" htmlType="submit" onClick={handleSubmit}>
          Atualizar
        </Button>
      </Form.Item>

      <Modal
        open={open}
        onCancel={() => setUpdateFormKid(false)}
      >
        <div>
          <p>
            Deseja realmente excluir o perfil?
          </p>
          <Button type="danger" onClick={deletedKid}>Sim</Button>
        </div>
      </Modal>
      <a key="delete" onClick={() => setOpen(true)}>Excluir perfil</a>

    </div>
  )
}

export default AccountKid;