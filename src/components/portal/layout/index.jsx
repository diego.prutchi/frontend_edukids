import HeaderHome from './HeaderHome';
import Footer from './Footer';
import Main from './Main';
import './style.css';

const Layout = ({ children }) => {

  return (

    <div>

      <HeaderHome />
      <Main>
        {children}
      </Main>
      <Footer />

    </div>

  )
}

export default Layout;