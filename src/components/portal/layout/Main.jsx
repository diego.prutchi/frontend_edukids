import './style.css'


const Main = ({ children}) => {

  return (

    <main className="main-container">
      <div>
       {children}
      </div>
    </main>
  )

}

export default Main;