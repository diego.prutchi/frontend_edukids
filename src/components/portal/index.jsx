import HeaderHome from './layout/HeaderHome';
import Main from './layout/Main';
import Footer from './layout/Footer';

const index = ({ children }) => {

  return (

    <div className="containerIndex">

      <HeaderHome />
      <Main>
        {children}
      </Main>
      <Footer />

    </div>

  )
}

export default index;