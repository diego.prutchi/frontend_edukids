import { Card, Avatar } from 'antd';

const { Meta } = Card;

const VideosList = (props) => {

  const cardsVideos = props.profileVideo.map((item, index) => {
    return (
      <Card
      onClick={() => props.onClickCard(item)}
      key={index}
        style={{ width: 300 }}
        cover={
          <img
            alt="thumb"
            src={item.videos.thumb_url}
          />
        }>
        <Meta
          title={item.videos.title}
          description={item.videos.description}
        />
        
      </Card>
    )
  })

  return (

    <>
    {cardsVideos}
    </>
  )

}

export default VideosList;