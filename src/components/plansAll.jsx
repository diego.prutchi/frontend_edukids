import { Card } from 'antd'
import './portal/layout/style.css'

const Plans = (props) => {
  return (
    <ul>

      {props.plans.map((plan, i) => {
        return <li key={i}>
          <Card className="PlansCard" title={plan.name} bordered={false}>
            <h3>{plan.term}</h3>
            <h3>{plan.concurrent}</h3>
            <h3>{plan.price}</h3>
            {props.subscriptionId === plan.id ? <p>Inscrito</p> : <p type="primary">Registrar</p>} 
          </Card>
        </li>
      })}
    </ul>

  )

}


export default Plans;
