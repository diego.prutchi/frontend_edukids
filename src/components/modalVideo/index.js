import { useDispatch, useSelector } from "react-redux"
import { Modal, Button } from 'antd';
import { MODAL } from "../../store/types";

const ModalContainer = () => {
  const dispatch = useDispatch();
  const {status, data} = useSelector((state) => state.modal);

  const toggleClose = () => dispatch({ type: MODAL.CLOSE, data: {} });

  // const remove = (id) => dispatch(removeCurso(id));


  return (
    <Modal 
      title="Basic Modal" 
      visible={status} 
      onCancel={toggleClose} 
      footer={null}
      >
      <p>{JSON.stringify(data)}</p>
      <button>Adicionar</button>
    </Modal>
  )
}

export default ModalContainer