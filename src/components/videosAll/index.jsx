import React, { useState, useEffect } from 'react'
import { Card, Divider } from 'antd';
import '../videosAll/style.css'
import Modal from "../modal"
import { addVideoKids } from "../../services/service.videos"
import { getAllKids } from "../../services/service.kids"
import { getUrlYoutubeVideo } from '../../utils/helpers'

import Kids from "../videosAll/Kids"

const { Meta } = Card;

function VideosAll(props) {

  const openModal = video => setModalData({ ...modalData, open: true, data: video })

  const [modalData, setModalData] = useState({
    open: false,
    data: {}
  })

  const [kidsData, setKidsData] = useState([])
  const [videoAuth, setVideoAuth] = useState({})

  const handleChange = (props) => {
    const { value, name } = props.target
    if (value == "-1") {
      return
    }
    setVideoAuth({
      ...videoAuth,
      kids_id: value,
      videos_id: modalData.data.id
    })
  }

  const cards = Object.keys(props.videos).map(function (parentalGuideString, i) {
    return (
      <>
      
      <li key={i}>
        <Divider orientation="left">
          {parentalGuideString}
        </Divider>
        <ul className="card-container ul-list-none">
          {props.videos[parentalGuideString].map(video =>
            <li key={video.id} className="card">
              <Card
                hoverable
                style={{ width: 240 }}
                cover={<img alt="example" src={video.thumb_url}
                  onMouseOver={e => (e.currentTarget.src = video.thumb_animated_url )}
                  onMouseOut={e => (e.currentTarget.src = video.thumb_url )}
                />}
                onClick={() => openModal(video)}

              >
                <Meta title={video.title} description={video.description} />
              </Card>
            </li>)}
        </ul>
      </li>
      </>
    )
  })

  useEffect(() => {
    async function teste() {
      const kids = await getAllKids()
      setKidsData(kids.data)
    }
    if (modalData.open) {
      setVideoAuth({
        ...videoAuth,
        videos_id: modalData.data.id
      })
      teste()
    }
  }, [modalData.open])

  return (
    <div className="main-container">

      <Modal
        open={modalData.open}

        onCancel={() => setModalData({ ...modalData, open: false })}>
        <div>
          <iframe id="ytplayer" type="text/html" width="480" height="260"
            src={getUrlYoutubeVideo(modalData.data?.video_url)}
            frameborder="2" />
          <div>
            <h3>Titulo: {modalData.data.title}</h3>
            <p>Descricao: {modalData.data.description}</p>
          </div>
          <p>parentalGuide: {modalData.data.parentalGuide}</p>
          <div>
            <p>language: {modalData.data.language}</p>
          </div>
          <div>
            {modalData.data.categories1?.category ? <p>category1: {modalData.data.categories1?.category}</p> : ""}
            {modalData.data.categories2?.category ? <p>category2: {modalData.data.categories2?.category}</p> : ""}
            {modalData.data.categories3?.category ? <p>category3: {modalData.data.categories3?.category}</p> : ""}
          </div>
          {props.includeAddKid &&
            <div>
              <select onChange={handleChange}>
                <option selected disabled value="-1">Selecione...</option>

                {kidsData.map((kid, i) =>
                  <option key={i} value={kid.id}>
                    {kid.nickname}
                  </option>
                )}
              </select>
              <div>
                <button onClick={() => { addVideoKids(videoAuth) }}>Adicionar</button>
              </div>
            </div>}


        </div>
      </Modal>
      <ul className="ul-list-none">
        {cards}
      </ul>
    </div>

  )
}

export { VideosAll }