import { Modal, Button } from 'antd';

const ModalContainer = (props) => {
  return (
    <Modal 
      title={props.title || ""} 
      visible={props.open} 
      onCancel={props.onCancel} 
      footer={null}
      >
    {props.children}
    </Modal>
  )
}

export default ModalContainer