import http from "../config/http"

export const dadosUser = (id) => http.get(`/users/${id}`)

export const updateUser = (id, body) => http.put(`/user/update/${id}`, body)

export const deleteUser = (id) => http.delete(`/users/${id}`)