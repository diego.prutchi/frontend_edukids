import http from "../config/http"

export const createUser = (data) => http.post("/users", data)