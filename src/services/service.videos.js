import http from "../config/http"

// Rotas para videos dos perfis

export const videosGetAll = (id) => http.get("/user/videos" + ( id ? `/${id}` : '' ))

export const addVideoKids = (data) => http.post("/auth_videos", data)

// Rotas para os vídeos

export const getAllVideos = () => http.get("/videos")

export const dadosVideo = () => http.get("/videos/:id")

export const addVideos = (data) => http.post("/videos" , data)

export const updateVideo= (id, body) => http.put(`/videos/${id}`, body)

export const deleteVideo = (id) => http.delete(`/videos/${id}`)